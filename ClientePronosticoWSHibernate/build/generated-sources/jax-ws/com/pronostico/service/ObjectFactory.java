
package com.pronostico.service;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.pronostico.service package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ParseException_QNAME = new QName("http://service.pronostico.com/", "ParseException");
    private final static QName _ConsultarPronostico_QNAME = new QName("http://service.pronostico.com/", "consultarPronostico");
    private final static QName _ConsultarPronosticoDias_QNAME = new QName("http://service.pronostico.com/", "consultarPronosticoDias");
    private final static QName _ConsultarPronosticoDiasResponse_QNAME = new QName("http://service.pronostico.com/", "consultarPronosticoDiasResponse");
    private final static QName _ConsultarPronosticoResponse_QNAME = new QName("http://service.pronostico.com/", "consultarPronosticoResponse");
    private final static QName _ConsultarPronosticoTiempo_QNAME = new QName("http://service.pronostico.com/", "consultarPronosticoTiempo");
    private final static QName _ConsultarPronosticoTiempoResponse_QNAME = new QName("http://service.pronostico.com/", "consultarPronosticoTiempoResponse");
    private final static QName _Hello_QNAME = new QName("http://service.pronostico.com/", "hello");
    private final static QName _HelloResponse_QNAME = new QName("http://service.pronostico.com/", "helloResponse");
    private final static QName _InsertarPronostico_QNAME = new QName("http://service.pronostico.com/", "insertarPronostico");
    private final static QName _InsertarPronosticoResponse_QNAME = new QName("http://service.pronostico.com/", "insertarPronosticoResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.pronostico.service
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ParseException }
     * 
     */
    public ParseException createParseException() {
        return new ParseException();
    }

    /**
     * Create an instance of {@link ConsultarPronostico }
     * 
     */
    public ConsultarPronostico createConsultarPronostico() {
        return new ConsultarPronostico();
    }

    /**
     * Create an instance of {@link ConsultarPronosticoDias }
     * 
     */
    public ConsultarPronosticoDias createConsultarPronosticoDias() {
        return new ConsultarPronosticoDias();
    }

    /**
     * Create an instance of {@link ConsultarPronosticoDiasResponse }
     * 
     */
    public ConsultarPronosticoDiasResponse createConsultarPronosticoDiasResponse() {
        return new ConsultarPronosticoDiasResponse();
    }

    /**
     * Create an instance of {@link ConsultarPronosticoResponse }
     * 
     */
    public ConsultarPronosticoResponse createConsultarPronosticoResponse() {
        return new ConsultarPronosticoResponse();
    }

    /**
     * Create an instance of {@link ConsultarPronosticoTiempo }
     * 
     */
    public ConsultarPronosticoTiempo createConsultarPronosticoTiempo() {
        return new ConsultarPronosticoTiempo();
    }

    /**
     * Create an instance of {@link ConsultarPronosticoTiempoResponse }
     * 
     */
    public ConsultarPronosticoTiempoResponse createConsultarPronosticoTiempoResponse() {
        return new ConsultarPronosticoTiempoResponse();
    }

    /**
     * Create an instance of {@link Hello }
     * 
     */
    public Hello createHello() {
        return new Hello();
    }

    /**
     * Create an instance of {@link HelloResponse }
     * 
     */
    public HelloResponse createHelloResponse() {
        return new HelloResponse();
    }

    /**
     * Create an instance of {@link InsertarPronostico }
     * 
     */
    public InsertarPronostico createInsertarPronostico() {
        return new InsertarPronostico();
    }

    /**
     * Create an instance of {@link InsertarPronosticoResponse }
     * 
     */
    public InsertarPronosticoResponse createInsertarPronosticoResponse() {
        return new InsertarPronosticoResponse();
    }

    /**
     * Create an instance of {@link Pronostico }
     * 
     */
    public Pronostico createPronostico() {
        return new Pronostico();
    }

    /**
     * Create an instance of {@link PronosticoId }
     * 
     */
    public PronosticoId createPronosticoId() {
        return new PronosticoId();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ParseException }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link ParseException }{@code >}
     */
    @XmlElementDecl(namespace = "http://service.pronostico.com/", name = "ParseException")
    public JAXBElement<ParseException> createParseException(ParseException value) {
        return new JAXBElement<ParseException>(_ParseException_QNAME, ParseException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConsultarPronostico }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link ConsultarPronostico }{@code >}
     */
    @XmlElementDecl(namespace = "http://service.pronostico.com/", name = "consultarPronostico")
    public JAXBElement<ConsultarPronostico> createConsultarPronostico(ConsultarPronostico value) {
        return new JAXBElement<ConsultarPronostico>(_ConsultarPronostico_QNAME, ConsultarPronostico.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConsultarPronosticoDias }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link ConsultarPronosticoDias }{@code >}
     */
    @XmlElementDecl(namespace = "http://service.pronostico.com/", name = "consultarPronosticoDias")
    public JAXBElement<ConsultarPronosticoDias> createConsultarPronosticoDias(ConsultarPronosticoDias value) {
        return new JAXBElement<ConsultarPronosticoDias>(_ConsultarPronosticoDias_QNAME, ConsultarPronosticoDias.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConsultarPronosticoDiasResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link ConsultarPronosticoDiasResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://service.pronostico.com/", name = "consultarPronosticoDiasResponse")
    public JAXBElement<ConsultarPronosticoDiasResponse> createConsultarPronosticoDiasResponse(ConsultarPronosticoDiasResponse value) {
        return new JAXBElement<ConsultarPronosticoDiasResponse>(_ConsultarPronosticoDiasResponse_QNAME, ConsultarPronosticoDiasResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConsultarPronosticoResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link ConsultarPronosticoResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://service.pronostico.com/", name = "consultarPronosticoResponse")
    public JAXBElement<ConsultarPronosticoResponse> createConsultarPronosticoResponse(ConsultarPronosticoResponse value) {
        return new JAXBElement<ConsultarPronosticoResponse>(_ConsultarPronosticoResponse_QNAME, ConsultarPronosticoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConsultarPronosticoTiempo }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link ConsultarPronosticoTiempo }{@code >}
     */
    @XmlElementDecl(namespace = "http://service.pronostico.com/", name = "consultarPronosticoTiempo")
    public JAXBElement<ConsultarPronosticoTiempo> createConsultarPronosticoTiempo(ConsultarPronosticoTiempo value) {
        return new JAXBElement<ConsultarPronosticoTiempo>(_ConsultarPronosticoTiempo_QNAME, ConsultarPronosticoTiempo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConsultarPronosticoTiempoResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link ConsultarPronosticoTiempoResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://service.pronostico.com/", name = "consultarPronosticoTiempoResponse")
    public JAXBElement<ConsultarPronosticoTiempoResponse> createConsultarPronosticoTiempoResponse(ConsultarPronosticoTiempoResponse value) {
        return new JAXBElement<ConsultarPronosticoTiempoResponse>(_ConsultarPronosticoTiempoResponse_QNAME, ConsultarPronosticoTiempoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Hello }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link Hello }{@code >}
     */
    @XmlElementDecl(namespace = "http://service.pronostico.com/", name = "hello")
    public JAXBElement<Hello> createHello(Hello value) {
        return new JAXBElement<Hello>(_Hello_QNAME, Hello.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link HelloResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link HelloResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://service.pronostico.com/", name = "helloResponse")
    public JAXBElement<HelloResponse> createHelloResponse(HelloResponse value) {
        return new JAXBElement<HelloResponse>(_HelloResponse_QNAME, HelloResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InsertarPronostico }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link InsertarPronostico }{@code >}
     */
    @XmlElementDecl(namespace = "http://service.pronostico.com/", name = "insertarPronostico")
    public JAXBElement<InsertarPronostico> createInsertarPronostico(InsertarPronostico value) {
        return new JAXBElement<InsertarPronostico>(_InsertarPronostico_QNAME, InsertarPronostico.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InsertarPronosticoResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link InsertarPronosticoResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://service.pronostico.com/", name = "insertarPronosticoResponse")
    public JAXBElement<InsertarPronosticoResponse> createInsertarPronosticoResponse(InsertarPronosticoResponse value) {
        return new JAXBElement<InsertarPronosticoResponse>(_InsertarPronosticoResponse_QNAME, InsertarPronosticoResponse.class, null, value);
    }

}
