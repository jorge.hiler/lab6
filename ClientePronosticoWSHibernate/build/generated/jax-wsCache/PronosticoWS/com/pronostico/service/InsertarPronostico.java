
package com.pronostico.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para insertarPronostico complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="insertarPronostico"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="temperatura_fahrenheit" type="{http://www.w3.org/2001/XMLSchema}float"/&gt;
 *         &lt;element name="temperatura_centigrados" type="{http://www.w3.org/2001/XMLSchema}float"/&gt;
 *         &lt;element name="porcentaje_humedad" type="{http://www.w3.org/2001/XMLSchema}float"/&gt;
 *         &lt;element name="tipo_nubosidad" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="probabilidad_precipitacion" type="{http://www.w3.org/2001/XMLSchema}float"/&gt;
 *         &lt;element name="fecha" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ciudad" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "insertarPronostico", propOrder = {
    "temperaturaFahrenheit",
    "temperaturaCentigrados",
    "porcentajeHumedad",
    "tipoNubosidad",
    "probabilidadPrecipitacion",
    "fecha",
    "ciudad"
})
public class InsertarPronostico {

    @XmlElement(name = "temperatura_fahrenheit")
    protected float temperaturaFahrenheit;
    @XmlElement(name = "temperatura_centigrados")
    protected float temperaturaCentigrados;
    @XmlElement(name = "porcentaje_humedad")
    protected float porcentajeHumedad;
    @XmlElement(name = "tipo_nubosidad")
    protected String tipoNubosidad;
    @XmlElement(name = "probabilidad_precipitacion")
    protected float probabilidadPrecipitacion;
    protected String fecha;
    protected String ciudad;

    /**
     * Obtiene el valor de la propiedad temperaturaFahrenheit.
     * 
     */
    public float getTemperaturaFahrenheit() {
        return temperaturaFahrenheit;
    }

    /**
     * Define el valor de la propiedad temperaturaFahrenheit.
     * 
     */
    public void setTemperaturaFahrenheit(float value) {
        this.temperaturaFahrenheit = value;
    }

    /**
     * Obtiene el valor de la propiedad temperaturaCentigrados.
     * 
     */
    public float getTemperaturaCentigrados() {
        return temperaturaCentigrados;
    }

    /**
     * Define el valor de la propiedad temperaturaCentigrados.
     * 
     */
    public void setTemperaturaCentigrados(float value) {
        this.temperaturaCentigrados = value;
    }

    /**
     * Obtiene el valor de la propiedad porcentajeHumedad.
     * 
     */
    public float getPorcentajeHumedad() {
        return porcentajeHumedad;
    }

    /**
     * Define el valor de la propiedad porcentajeHumedad.
     * 
     */
    public void setPorcentajeHumedad(float value) {
        this.porcentajeHumedad = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoNubosidad.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoNubosidad() {
        return tipoNubosidad;
    }

    /**
     * Define el valor de la propiedad tipoNubosidad.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoNubosidad(String value) {
        this.tipoNubosidad = value;
    }

    /**
     * Obtiene el valor de la propiedad probabilidadPrecipitacion.
     * 
     */
    public float getProbabilidadPrecipitacion() {
        return probabilidadPrecipitacion;
    }

    /**
     * Define el valor de la propiedad probabilidadPrecipitacion.
     * 
     */
    public void setProbabilidadPrecipitacion(float value) {
        this.probabilidadPrecipitacion = value;
    }

    /**
     * Obtiene el valor de la propiedad fecha.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFecha() {
        return fecha;
    }

    /**
     * Define el valor de la propiedad fecha.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFecha(String value) {
        this.fecha = value;
    }

    /**
     * Obtiene el valor de la propiedad ciudad.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCiudad() {
        return ciudad;
    }

    /**
     * Define el valor de la propiedad ciudad.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCiudad(String value) {
        this.ciudad = value;
    }

}
