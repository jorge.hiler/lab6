
package com.pronostico.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para pronostico complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="pronostico"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="id" type="{http://service.pronostico.com/}pronosticoId" minOccurs="0"/&gt;
 *         &lt;element name="porcentajeHumedad" type="{http://www.w3.org/2001/XMLSchema}float" minOccurs="0"/&gt;
 *         &lt;element name="probabilidadPrecipitacion" type="{http://www.w3.org/2001/XMLSchema}float" minOccurs="0"/&gt;
 *         &lt;element name="temperaturaCentigrados" type="{http://www.w3.org/2001/XMLSchema}float" minOccurs="0"/&gt;
 *         &lt;element name="temperaturaFahrenheit" type="{http://www.w3.org/2001/XMLSchema}float" minOccurs="0"/&gt;
 *         &lt;element name="tipoNubosidad" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "pronostico", propOrder = {
    "id",
    "porcentajeHumedad",
    "probabilidadPrecipitacion",
    "temperaturaCentigrados",
    "temperaturaFahrenheit",
    "tipoNubosidad"
})
public class Pronostico {

    protected PronosticoId id;
    protected Float porcentajeHumedad;
    protected Float probabilidadPrecipitacion;
    protected Float temperaturaCentigrados;
    protected Float temperaturaFahrenheit;
    protected String tipoNubosidad;

    /**
     * Obtiene el valor de la propiedad id.
     * 
     * @return
     *     possible object is
     *     {@link PronosticoId }
     *     
     */
    public PronosticoId getId() {
        return id;
    }

    /**
     * Define el valor de la propiedad id.
     * 
     * @param value
     *     allowed object is
     *     {@link PronosticoId }
     *     
     */
    public void setId(PronosticoId value) {
        this.id = value;
    }

    /**
     * Obtiene el valor de la propiedad porcentajeHumedad.
     * 
     * @return
     *     possible object is
     *     {@link Float }
     *     
     */
    public Float getPorcentajeHumedad() {
        return porcentajeHumedad;
    }

    /**
     * Define el valor de la propiedad porcentajeHumedad.
     * 
     * @param value
     *     allowed object is
     *     {@link Float }
     *     
     */
    public void setPorcentajeHumedad(Float value) {
        this.porcentajeHumedad = value;
    }

    /**
     * Obtiene el valor de la propiedad probabilidadPrecipitacion.
     * 
     * @return
     *     possible object is
     *     {@link Float }
     *     
     */
    public Float getProbabilidadPrecipitacion() {
        return probabilidadPrecipitacion;
    }

    /**
     * Define el valor de la propiedad probabilidadPrecipitacion.
     * 
     * @param value
     *     allowed object is
     *     {@link Float }
     *     
     */
    public void setProbabilidadPrecipitacion(Float value) {
        this.probabilidadPrecipitacion = value;
    }

    /**
     * Obtiene el valor de la propiedad temperaturaCentigrados.
     * 
     * @return
     *     possible object is
     *     {@link Float }
     *     
     */
    public Float getTemperaturaCentigrados() {
        return temperaturaCentigrados;
    }

    /**
     * Define el valor de la propiedad temperaturaCentigrados.
     * 
     * @param value
     *     allowed object is
     *     {@link Float }
     *     
     */
    public void setTemperaturaCentigrados(Float value) {
        this.temperaturaCentigrados = value;
    }

    /**
     * Obtiene el valor de la propiedad temperaturaFahrenheit.
     * 
     * @return
     *     possible object is
     *     {@link Float }
     *     
     */
    public Float getTemperaturaFahrenheit() {
        return temperaturaFahrenheit;
    }

    /**
     * Define el valor de la propiedad temperaturaFahrenheit.
     * 
     * @param value
     *     allowed object is
     *     {@link Float }
     *     
     */
    public void setTemperaturaFahrenheit(Float value) {
        this.temperaturaFahrenheit = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoNubosidad.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoNubosidad() {
        return tipoNubosidad;
    }

    /**
     * Define el valor de la propiedad tipoNubosidad.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoNubosidad(String value) {
        this.tipoNubosidad = value;
    }

}
