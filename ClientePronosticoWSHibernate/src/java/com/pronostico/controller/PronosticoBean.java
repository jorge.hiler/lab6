/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pronostico.controller;

import com.pronostico.service.ParseException_Exception;
import com.pronostico.service.Pronostico;
import com.pronostico.service.PronosticoWS_Service;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.annotation.ManagedBean;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.xml.ws.WebServiceRef;

/**
 *
 * @author W
 */

//@ManagedBean
//@SessionScoped
public class PronosticoBean {

    @WebServiceRef(wsdlLocation = "WEB-INF/wsdl/localhost_8080/pronosticoWSHibertnate/PronosticoWS.wsdl")
    private PronosticoWS_Service service;

    private String ciudad;
    private String fecha;
    private Float temperaturaFahrenheit;
    private Float temperaturaCentigrados;
    private Float porcentajeHumedad;
    private String tipoNubosidad;
    private Float probabilidadPrecipitacion;
    private ArrayList<Pronostico> listaPronostico;

    /**
     * Creates a new instance of PronosticoBean
     */
    public PronosticoBean() {
    }

    


    public String guardaPronostico() throws ParseException_Exception {
        String msj = insertarPronostico(temperaturaFahrenheit, temperaturaCentigrados, porcentajeHumedad, tipoNubosidad, probabilidadPrecipitacion, fecha, ciudad);
        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, msj, "...");
        FacesContext.getCurrentInstance().addMessage(null, msg);
        limpiarForm();
        return "index";

    }
    
    
        public String consultarPronosticoC() throws ParseException_Exception {
        String msj = consultarPronosticoTiempo(fecha, ciudad);
        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, msj, "...");
        FacesContext.getCurrentInstance().addMessage(null, msg);
        limpiarForm();
        return "index";

    }
    

    private String consultarPronosticoTiempo(java.lang.String fecha, java.lang.String ciudad) throws ParseException_Exception {
        // Note that the injected javax.xml.ws.Service reference as well as port objects are not thread safe.
        // If the calling of port operations may lead to race condition some synchronization is required.
        com.pronostico.service.PronosticoWS port = service.getPronosticoWSPort();
        return port.consultarPronosticoTiempo(fecha, ciudad);
    }

    private String insertarPronostico(float temperaturaFahrenheit, float temperaturaCentigrados, float porcentajeHumedad, java.lang.String tipoNubosidad, float probabilidadPrecipitacion, java.lang.String fecha, java.lang.String ciudad) throws ParseException_Exception {
        // Note that the injected javax.xml.ws.Service reference as well as port objects are not thread safe.
        // If the calling of port operations may lead to race condition some synchronization is required.
        com.pronostico.service.PronosticoWS port = service.getPronosticoWSPort();
        return port.insertarPronostico(temperaturaFahrenheit, temperaturaCentigrados, porcentajeHumedad, tipoNubosidad, probabilidadPrecipitacion, fecha, ciudad);
    }

    public PronosticoWS_Service getService() {
        return service;
    }

    public void setService(PronosticoWS_Service service) {
        this.service = service;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public Float getTemperaturaFahrenheit() {
        return temperaturaFahrenheit;
    }

    public void setTemperaturaFahrenheit(Float temperaturaFahrenheit) {
        this.temperaturaFahrenheit = temperaturaFahrenheit;
    }

    public Float getTemperaturaCentigrados() {
        return temperaturaCentigrados;
    }

    public void setTemperaturaCentigrados(Float temperaturaCentigrados) {
        this.temperaturaCentigrados = temperaturaCentigrados;
    }

    public Float getPorcentajeHumedad() {
        return porcentajeHumedad;
    }

    public void setPorcentajeHumedad(Float porcentajeHumedad) {
        this.porcentajeHumedad = porcentajeHumedad;
    }

    public String getTipoNubosidad() {
        return tipoNubosidad;
    }

    public void setTipoNubosidad(String tipoNubosidad) {
        this.tipoNubosidad = tipoNubosidad;
    }

    public Float getProbabilidadPrecipitacion() {
        return probabilidadPrecipitacion;
    }

    public void setProbabilidadPrecipitacion(Float probabilidadPrecipitacion) {
        this.probabilidadPrecipitacion = probabilidadPrecipitacion;
    }



    private void limpiarForm() {

        this.ciudad = "";
        this.fecha = "";
        this.temperaturaFahrenheit = 0.0f;
        this.temperaturaCentigrados = 0.0f;
        this.porcentajeHumedad = 0.0f;
        this.tipoNubosidad = "";
        this.probabilidadPrecipitacion = 0.0f;

    }

}
