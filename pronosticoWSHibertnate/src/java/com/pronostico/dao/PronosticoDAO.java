/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pronostico.dao;

import com.pronostico.persistencia.Pronostico;
import com.pronostico.persistencia.PronosticoId;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import net.sf.ehcache.hibernate.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

/**
 *
 * @author W
 */
public class PronosticoDAO {

    public void ingresarPronostico(Pronostico p) {
        SessionFactory sf = null;
        Session sesion = null;
        Transaction tx = null;
        try {
            sf = NewHibernateUtil.getSessionFactory();
            sesion = sf.openSession();
            tx = sesion.beginTransaction();
            sesion.save(p);
            tx.commit();
            sesion.close();
        } catch (Exception e) {
            tx.rollback();
            throw new RuntimeException("No se ingresar el pronostico");
        }
    }



    public String consultarPronostico(String ciudad, String fecha) throws ParseException {
        SessionFactory sf = NewHibernateUtil.getSessionFactory();
        Session sesion = sf.openSession();
        SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
        Date fechas = formato.parse(fecha);
        Pronostico pHoy = (Pronostico) sesion.get(Pronostico.class, new PronosticoId(ciudad, fechas));

        //  sesion.close();
        String result = "";

        if (pHoy != null) {         
            result = result + "El pronostico de ciudad " + ciudad + " para el dia: " + fecha.toString() + "  Temperatura fatrenheit: " + pHoy.getTemperaturaFahrenheit() + " Temperatura centigrados: " + pHoy.getTemperaturaCentigrados() + " Porcentaje de humedad: " + pHoy.getPorcentajeHumedad() + "% Tipo Nubosidad: " + pHoy.getTipoNubosidad() + " Probabilidad de precipitacion: " + pHoy.getProbabilidadPrecipitacion() + "%";
            for (int i = 1; i <= 3; i++) {
                Pronostico pd = (Pronostico) sesion.get(Pronostico.class, new PronosticoId(ciudad, sumarDiasFecha(fechas, i)));
                if (pd != null) {
                    result = result + "*****Dia: " + pd.getId().getFechaCadena() + "  Temperatura fatrenheit: " + pd.getTemperaturaFahrenheit() + " Temperatura centigrados: " + pd.getTemperaturaCentigrados() + " Porcentaje de humedad: " + pd.getPorcentajeHumedad() + "% Tipo Nubosidad: " + pd.getTipoNubosidad() + " Probabilidad de precipitacion: " + pd.getProbabilidadPrecipitacion() + "%";
                } else {
                   PronosticoId f = new PronosticoId("", sumarDiasFecha(fechas, i));
                   result = result + "*****Dia: " + f.getFechaCadena()  + " no se encuentra en DB";
                }
            }
            return result;
        } else {
            return "El pronostico de ciudad " + ciudad + " para el dia " + fecha.toString() + " no se encuentra disponible";
        }
    }

    public ArrayList<Pronostico> consultarPronosticoDias(String ciudad, String fecha) throws ParseException {
        SessionFactory sf = NewHibernateUtil.getSessionFactory();
        Session sesion = sf.openSession();
        SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");

        Date fechas = formato.parse(fecha);
        Pronostico pHoy = (Pronostico) sesion.get(Pronostico.class, new PronosticoId(ciudad, fechas));
        Pronostico pd1 = (Pronostico) sesion.get(Pronostico.class, new PronosticoId(ciudad, sumarDiasFecha(fechas, 1)));
        Pronostico pd2 = (Pronostico) sesion.get(Pronostico.class, new PronosticoId(ciudad, sumarDiasFecha(fechas, 2)));
        Pronostico pd3 = (Pronostico) sesion.get(Pronostico.class, new PronosticoId(ciudad, sumarDiasFecha(fechas, 3)));

        ArrayList<Pronostico> pdias = new ArrayList<Pronostico>();

        sesion.close();
        if (pHoy != null) {
            pdias.add(pHoy);
        }
        if (pd1 != null) {
            pdias.add(pd1);
        }
        if (pd2 != null) {
            pdias.add(pd2);
        }
        if (pd3 != null) {
            pdias.add(pd3);
        }

        return pdias;
    }

    public Date sumarDiasFecha(Date fecha, int dias) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(fecha); // Configuramos la fecha que se recibe	
        calendar.add(Calendar.HOUR, 24 * dias);  // numero de horas a añadir, o restar en caso de horas<0	
        Date date = calendar.getTime(); 
        return date; // Devuelve el objeto Date con las nuevas horas añadidas	 

    }

}
