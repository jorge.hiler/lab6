/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pronostico.service;

import com.pronostico.dao.PronosticoDAO;
import com.pronostico.persistencia.Pronostico;
import com.pronostico.persistencia.PronosticoId;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.xml.bind.annotation.XmlSeeAlso;

/**
 *
 * @author W
 */
@WebService(serviceName = "PronosticoWS")
@XmlSeeAlso({Pronostico.class})
public class PronosticoWS {



    /**
     * Web service operation
     */
    @WebMethod(operationName = "consultarPronosticoTiempo")
    public String consultarPronosticoTiempo(@WebParam(name = "fecha") String fecha, @WebParam(name = "ciudad") String ciudad) throws ParseException {
        PronosticoDAO pronosticoDOA = new PronosticoDAO();
        return pronosticoDOA.consultarPronostico(ciudad, fecha);
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "insertarPronostico")
    public String insertarPronostico(@WebParam(name = "temperatura_fahrenheit") float temperatura_fahrenheit, @WebParam(name = "temperatura_centigrados") float temperatura_centigrados, @WebParam(name = "porcentaje_humedad") float porcentaje_humedad, @WebParam(name = "tipo_nubosidad") String tipo_nubosidad, @WebParam(name = "probabilidad_precipitacion") float probabilidad_precipitacion, @WebParam(name = "fecha") String fecha, @WebParam(name = "ciudad") String ciudad) throws ParseException {
        //TODO write your implementation code here:
        SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");        
        PronosticoId id = new PronosticoId(ciudad, formato.parse(fecha));
        Pronostico p= new Pronostico(id, temperatura_fahrenheit, temperatura_centigrados, porcentaje_humedad, tipo_nubosidad, probabilidad_precipitacion);
        PronosticoDAO pronosticoDAO = new PronosticoDAO();
        pronosticoDAO.ingresarPronostico(p);
        return "Pronostico ingresado";
    }

 

}
